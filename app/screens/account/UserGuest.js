import React from "react"
import {View, Text, ScrollView, Image, StyleSheet, Dimensions} from "react-native"
import {Button} from "react-native-elements"
import {useNavigation} from "@react-navigation/native";
import {userGuestImage} from "../../../assets/img";


export default function UserGuest() {

    const navigation = useNavigation();

    console.log( Dimensions.get("window").height )

    return (
      <ScrollView centerContent={true} style={styles.viewBody}>
          <Image
            source={userGuestImage}
            resizeMode="contain"
            style={styles.image}
           />
          <Text style={styles.title}>Consulta tu perfil de 5 Tenedores</Text>
          <Text style={styles.description}>
              Elije el mejor restaurante de tu preferencia.
          </Text>
          <View style={styles.viewBtn}>
            <Button
                title="Ver tu perfil"
                buttonStyle={styles.btnStyle}
                // containerStyle={}
                onPress={()=> navigation.navigate('login') }
            />
          </View>
      </ScrollView>
    );
  }

const styles = StyleSheet.create({
    viewBody:{
        // marginLeft:30,
        // marginRight:30
    },
    image:{
        height: (Dimensions.get("window").height - ( Dimensions.get("window").height * 0.60) ),
        // height: 240,
        width:"100%",
        marginTop: 30,
        marginBottom: 30
    },
    title:{
        fontWeight:"bold",
        fontSize:19,
        marginBottom: 10,
        textAlign: "center"
    },
    description:{
        fontWeight:"bold",
        fontSize:19,
        marginBottom: 20,
        textAlign: "center"
    },
    viewBtn:{
       flex:1,
       alignItems:"center"
    },
    btnStyle:{
        backgroundColor:"#00a680"
    },
    btnContainer:{
        width:"70%"
    }
});