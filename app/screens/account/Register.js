import React, {useRef} from "react"
import { View, Image, StyleSheet} from "react-native"
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Toast from "react-native-easy-toast"

import {logoImage} from "../../../assets/img";
import RegisterForm from "../../components/account/RegisterForm";

export default function Register() {
    const toastRef = useRef()
    return(
        <KeyboardAwareScrollView>
            <Image
                source={logoImage}
                resizeMode="contain"
                style={styles.logo}
            />
            <View style={styles.viewContainer}>
                <RegisterForm toastRef={toastRef}/>
            </View>
            <Toast
                ref={toastRef}
                position="center"
                opacity={0.9}
                useNativeDriver={true}
                fadeOutDuration={3000}
            />
        </KeyboardAwareScrollView>
    )
}

const styles = StyleSheet.create({
    logo:{
        height:150,
        width:"100%",
        marginTop:20
    },
    viewContainer:{
        marginLeft: 25,
        marginRight: 25
    },
    textRegister:{
        marginTop: 15,
        marginLeft: 10,
        marginRight: 10,
        paddingEnd: 15
    },
    btnRegister:{
        color:"#00a680",
        fontWeight:"bold",
    },
    divide:{
        margin: 40,
        backgroundColor: "#00a680"
    }
})