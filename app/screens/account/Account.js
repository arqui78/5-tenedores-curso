import React, {useState, useEffect} from "react"
import {View, Text, StyleSheet} from "react-native"
import * as  firebase from "firebase";
import Loading    from "../../components/commos/Loading";
import UserGuest  from "./UserGuest";
import UserLogged from "./UserLogged";



export default function Account(props = {}) {
    const [userAuth, setUserAuth] = useState(null);

    useEffect( () =>{
        firebase.auth().onAuthStateChanged(user => {
            setUserAuth(user ? user : false)
        })
    }, [])

    if( userAuth == null) return <Loading isVisible={true} text="Cargando..."/>

    return(
        <View style={styles.viewBody}>
            {
                userAuth ? <UserLogged user={userAuth} {...props}/> : <UserGuest/>
            }
        </View>
    )
  }
const styles = StyleSheet.create({
    viewBody:{
        flex:1,
        backgroundColor:"#fff",
        // width: "100%",
    }
});