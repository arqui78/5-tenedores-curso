import React, {useRef, useState, useEffect} from "react"
import {View, StyleSheet} from "react-native"
import {Button} from "react-native-elements"
import Toast from "react-native-easy-toast"
import * as firebase from "firebase"

import Loading from "../../components/commos/Loading";
import InfoUser from "../../components/account/InfoUser";
import AccountOptions from "../../components/account/AccountOptions";

export default function UserLogged() {

    const toastRef = useRef();

    const [ userInfo, setUserInfo ] = useState(null);
    const [ loading, setLoading ] = useState(false);
    const [ loadingText, setLoadingText ] = useState('');
    const [ reloadUserInfo, setReloadUserInfo ] = useState(false);

    useEffect(() =>{
        ( async ()=>{
           const user = await firebase.auth().currentUser;
            setUserInfo(user)
        })();
        setReloadUserInfo(false)
    }, [reloadUserInfo])

    return (
      <View style={styles.viewUserInfo}>
          {
              userInfo && <InfoUser
                  userInfo={userInfo}
                  toastRef={toastRef}
                  setLoading={setLoading}
                  setLoadingText={setLoadingText}
              />
          }

          <AccountOptions
              userInfo={userInfo}
              toastRef={toastRef}
              setReloadUserInfo={setReloadUserInfo}
          />

          <Button
              title="Cerrar sesión"
              titleStyle={styles.btnCloseSessionText}
              buttonStyle={styles.btnCloseSession}
              onPress={() => firebase.auth().signOut() }
          />
          <Toast
              ref={toastRef}
              position="center"
              opacity={0.9}
              fadeOutDuration={3000}
          />
          <Loading isVisible={loading} text={loadingText}/>
      </View>
    );
  }

const styles = StyleSheet.create({
    viewUserInfo:{
        // flex:1,
        // // minHeight:"100%",
        // backgroundColor: "#f2f2f2"
    },
    inputForm:{
        width:"100%",
        marginTop: 20
    },
    btnCloseSession:{
        marginTop: 30,
        borderRadius: 0,
        backgroundColor: "#fff",
        borderTopColor:"#fff",
        borderTopWidth:1,
        borderBottomWidth: 1,
        borderBottomColor:"#fff",
        paddingTop: 10,
        paddingBottom: 10

    },
    btnCloseSessionText:{
        color:"#00a680"
    },
    iconRight:{
        color:"#c1c1c1"
    }
});