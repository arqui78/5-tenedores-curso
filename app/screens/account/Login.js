import React, {useRef} from "react"
import { View, Text, ScrollView, Image, StyleSheet} from "react-native"
import { Divider} from "react-native-elements"
import Toast from "react-native-easy-toast"

import {useNavigation} from "@react-navigation/native";
import LoginForm from "../../components/account/LoginForm";
import LoginFormFacebook from "../../components/account/LoginFacebook";

import {logoImage} from "../../../assets/img";
import styles_css from "../../../app.css"

export default function Login() {
    const toastRef = useRef();
    return(
        <ScrollView>
            <Image
                source={logoImage}
                resizeMode="contain"
                style={styles.logo}
            />
            <View style={styles.viewContainer}>
                <LoginForm toastRef={toastRef}/>
                <CreateAccount/>
            </View>
            <Divider style={styles.divide}/>
            <View style={styles.viewContainer}>
                <LoginFormFacebook toastRef={toastRef}/>
            </View>
            <Toast
                ref={toastRef}
                position="center"
                opacity={0.9}
                fadeOutDuration={3000}
            />
        </ScrollView>
    )
}

function CreateAccount() {
    const navigation = useNavigation()
    return(
        <Text style={styles.textRegister}>
            { "Aun no tines una Cuenta? "}
            <Text
                className={styles_css.fontPrimary}
                // style={styles.btnRegister}
                onPress={()=> navigation.navigate('register') }
            >Registrate</Text>
        </Text>
    )
}

const styles = StyleSheet.create({
    logo:{
        height:150,
        width:"100%",
        marginTop:20
    },
    viewContainer:{
        // flex:1,
        // alignItems:"center",
        marginLeft: 25,
        marginRight: 25
    },
    textRegister:{
        marginTop: 15,
        marginLeft: 10,
        marginRight: 10,
        paddingEnd: 15
    },
    btnRegister:{
        color:"#00a680",
        fontWeight:"bold",
    },
    divide:{
        margin: 40,
        backgroundColor: "#00a680"
    }
})