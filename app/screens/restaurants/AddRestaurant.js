import React, {useRef, useState} from "react"
import {View} from "react-native"


import Toast from "react-native-easy-toast"
import Loading from "../../components/commos/Loading";
import AddRestaurantForm from "../../components/restaurants/AddRestaurantForm";

export default function AddRestaurant(props) {
    const {navigation} = props;
    const toastRef = useRef();

    const [ loading, setLoading ] = useState(false);

    return (
      <View>
          <AddRestaurantForm
              navigation={navigation}
              toastRef={toastRef}
              setLoading={setLoading}
          />
          <Toast
              ref={toastRef}
              position="center"
              opacity={0.9}
              fadeOutDuration={3000}
          />
          <Loading isVisible={loading} text="Creando restaurante"/>
      </View>
    );
  }
