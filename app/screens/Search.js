import React, {useEffect, useState} from "react"
import {View, Text, StyleSheet, ActivityIndicator, TouchableOpacity, FlatList} from "react-native"
import {Icon, Image, ListItem, SearchBar} from "react-native-elements";
import {FireSQL} from "firesql"
import {throttle} from 'lodash';

import {firebaseApp} from "../utils/firebase";
import firebase from "firebase/app"
import {notImage, notResultFound} from "../../assets/img"


const fireSQL = new FireSQL(firebase.firestore(firebaseApp), {includeId:"id"});

export default function Search(props) {
    const {navigation} = props;
    const [ search, setSearch ] = useState("");
    const [ restaurants, setRestaurants ] = useState([]);

    const fetchData = React.useMemo(
        () =>
            throttle((query, callback) => {
                fireSQL.query(query)
                    .then(response => callback(null, response) )
                    .catch(callback)

            }, 200),
        [],
    );

    useEffect(()=>{
        if(search){
            const query = `SELECT * FROM restaurants WHERE name LIKE '${search}%'`
            fireSQL.query(query)
                .then( response => setRestaurants(response) )
                .catch(() => setRestaurants([]))
            // fetchData(query, (err, results) => {
            //     if(err) {
            //         console.log(err)
            //         setRestaurants([])
            //     }else{
            //         setRestaurants(results)
            //     }
            // })
        }else{
            setRestaurants([])
        }

    }, [search])

    return (
      <View style={styles.viewBody}>
        <SearchBar
            placeholder="Buscar restaurantes..."
            onChangeText={setSearch}
            value={search}
            containerStyle={styles.searchBar}
        />
          {
              restaurants.length > 0 ? (
                  <View>
                      <FlatList
                          data={restaurants}
                          renderItem={ restaurant => <Restaurant
                              restaurant={restaurant}
                              navigation={navigation}
                          />}
                          keyExtractor={(item, index) => index.toString() }
                      />
                  </View>
              ) : (
                  <View style={styles.viewContainer}>
                      <Image
                          resizeMode="cover"
                          PlaceholderContent={<ActivityIndicator color="#fff"/>}
                          style={styles.notResultFound}
                          source={notResultFound}
                      />
                  </View>

              )
          }
      </View>
    );
  }
function Restaurant(props){
    const {restaurant, navigation} = props
    const {id, name, images} = restaurant.item
    const imageRestaurant = images[0];

    const goNavigation = () => {
        navigation.navigate('restaurant', {id, name})

    }

    return(
        <ListItem
            title={name}
            leftAvatar={{
                source: imageRestaurant ?  {uri:imageRestaurant} : notImage
            }}
            rightIcon={<Icon type="material-community" name="chevron-right"/>}
            onPress={goNavigation}
        />
    );

}

const styles = StyleSheet.create({
    viewBody:{
        flex:1,
        backgroundColor:"#f2f2f2f2"
    },
    searchBar:{
        marginBottom:20
    },
    viewContainer:{
        flex:1,
        alignItems:"center",
    },
    notResultFound:{
        width:200,
        height:200
    }
})