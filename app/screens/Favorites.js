import React, {useCallback, useRef, useState} from "react"
import {View, Text, StyleSheet, FlatList, ActivityIndicator, TouchableOpacity, Alert, ScrollView} from "react-native"
import {useFocusEffect} from "@react-navigation/native"
import {Button, Icon, Image} from "react-native-elements";

import {firebaseApp} from "../utils/firebase";
import firebase from "firebase/app"
import "firebase/storage"
import Loading from "../components/commos/Loading";
import {notImage} from "../../assets/img";
import Toast from "react-native-easy-toast";


const db = firebase.firestore(firebaseApp);

export default function Favorites(props) {

    const {navigation} = props;

    const [ restaurants, setRestaurants ] = useState(null);
    const [userLogged, setUserLogged] = useState(false);
    const [isRemoveRestaurant, setIsRemoveRestaurant] = useState(false)
    const [reloadData, setReloadData] = useState(null)
    const [ isLoading, setIsLoading ] = useState(true);

    const toastRef = useRef();

    firebase.auth().onAuthStateChanged((user) => user ? setUserLogged(true) : setUserLogged(false) );

    useFocusEffect(
        useCallback(() => {
            if(userLogged){
                db.collection("favorites")
                    .where("idUser", "==", firebase.auth().currentUser.uid)
                    .get()
                    .then( response => {

                        if(response.docs.length > 0){
                            const idRestaurantsArray = [];
                            response.docs.forEach(doc => idRestaurantsArray.push(doc.data().idRestaurant));
                            getRestaurantsData(idRestaurantsArray).then(results =>{
                                const resultRestaurants = [];
                                results.forEach(doc => {
                                    const restaurant = doc.data();
                                    restaurant.id = doc.id;
                                    resultRestaurants.push( restaurant )
                                })
                                setIsLoading(false)
                                setRestaurants(resultRestaurants)
                            })
                        }else{
                            setIsLoading(false)
                            setRestaurants([])
                        }

                    }).catch(error =>{
                        console.error(error)
                        setIsLoading(false)
                })
            }

        }, [userLogged, reloadData])
    );


    const getRestaurantsData = (idRestaurantsArray) => {
        const arrayRestaurants = [];
        idRestaurantsArray.forEach(idRestaurant => {
            const result = db.collection("restaurants").doc(idRestaurant).get();
            arrayRestaurants.push(result)
        })
        return Promise.all(arrayRestaurants)

    }

    if(!userLogged) return <UserNoLogged navigation={navigation}/>
    if( !isLoading && restaurants?.length === 0) return <NotFound/>

    return (
      <View style={styles.viewBody}>
          {restaurants ? (
              <View>
                  <FlatList
                      data={restaurants}
                      renderItem={ restaurant => <Restaurant
                          restaurant={restaurant}
                          setIsRemoveRestaurant={setIsRemoveRestaurant}
                          toastRef={toastRef}
                          setReloadData={setReloadData}
                          navigation={navigation}
                      />}
                      keyExtractor={(item, index) => index.toString() }
                  />
                  <Toast ref={toastRef} position="center" opacity={0.9} />
                  <Loading isVisible={isRemoveRestaurant} text='Eliminando restaurante'/>
              </View>
          ) : (
              <View style={styles.loadingRestaurants}>
                  <ActivityIndicator size="large"/>
                  <Text style={{textAlign:"center"}}>Cargando restaurantes favoritos</Text>
              </View>
          )}
      </View>
    );
  }

function NotFound(){
  return (
      <View style={styles.viewContainer}>
          <Icon type="material-community" size={50} name="alert-outline"/>
          <Text>No tiene restaurantes Favoritos</Text>
      </View>
  );
}

function UserNoLogged(props){

    const {navigation} = props;

    const onPress = () => {
        // navigation.navigate('account', {screen: "login", previous_screen:"Favorites"})
        navigation.navigate("login")
    }

    return (
        <View style={styles.viewContainer}>
            <Icon type="material-community" size={50} name="alert-outline"/>
            <Text style={{...styles.textFound, textAlign:'center'}}>Necesitas estar loguiado para ver esta seccion</Text>
            <Button
                title="Ir al login"
                containerStyle={{marginTop: 20, width: 120}}
                buttonStyle={{backgroundColor:"#00a680"}}
                onPress={onPress}
            />
        </View>
    );
}

function Restaurant(props){
    const {restaurant, setIsRemoveRestaurant, toastRef, setReloadData, navigation} = props
    const {id, name, images} = restaurant.item
    const imageRestaurant = images[0];

    const confirmRemoveFavorite = () => {
        Alert.alert(
            'Eliminar de Favoritos',
            `Estas seguro que quieres eliminar el restaurante ${name} de favoritos?`,
            [{
                text:'Cancelar',
                style:'cancel'
            },{
                text:'Eliminar',
                style:'default',
                onPress: actionRemoveFavorite
            }],
            {cancelable:false}
        )
    }

    const actionRemoveFavorite = () =>{
        setIsRemoveRestaurant(true)
        db.collection("favorites")
            .where("idRestaurant", "==", id)
            .where("idUser", "==", firebase.auth().currentUser.uid)
            .get()
            .then((response) => {
                response.forEach((doc) => {
                    const idFavorite = doc.id;
                    db.collection("favorites")
                        .doc(idFavorite)
                        .delete()
                        .then(() => {
                            setReloadData(new Date().getTime())
                            setIsRemoveRestaurant(false)
                            toastRef.current.show("Eliminado correctamente");
                        })
                        .catch(() => {
                            console.log('Error')
                            setIsRemoveRestaurant(false)
                            toastRef.current.show(
                                "Error al eliminar el restaurante de favoritos"
                            );
                        });
                });
            });

    }

    const goNavigation = () => {
        // navigation.navigate('restaurants', {screen: "restaurant", params:{id, name}})
        navigation.navigate('restaurant', {id, name})

    }

    return(
        <View
            style={styles.restaurant}
        >
            <TouchableOpacity onPress={goNavigation}>
                <Image
                    resizeMode="cover"
                    style={styles.image}
                    PlaceholderContent={<ActivityIndicator color="#fff"/>}
                    source={
                        imageRestaurant
                            ? {uri: imageRestaurant}
                            : notImage
                    }

                />
                <View style={styles.info}>
                    <Text style={styles.name}>{name}</Text>
                    <Icon
                        type="material-community"
                        name="heart"
                        color="#f00"
                        containerStyle={styles.favorite}
                        onPress={() => confirmRemoveFavorite()}
                        underlayColor="transparent"
                    />
                </View>
            </TouchableOpacity>
        </View>
    );

}

const styles = StyleSheet.create({
    viewContainer:{
        flex:1,
        alignItems:"center",
        justifyContent: "center"
    },
    textFound:{
        fontSize: 20,
        fontWeight: "bold"
    },
    viewBody:{
        flex:1,
        backgroundColor:"#f2f2f2f2"
    },
    loadingRestaurants:{
        marginTop: 10,
        marginBottom: 10
    },
    restaurant:{
        marginTop:10
    },
    image:{
        width: '100%',
        height: 180
    },
    info:{
        flex: 1,
        alignItems: "center",
        justifyContent: "space-between",
        flexDirection: "row",
        paddingLeft: 20,
        paddingRight: 20,
        paddingTop: 10,
        paddingBottom: 10,
        marginTop:-30,
        backgroundColor: "#fff"
    },
    name:{
        fontSize: 30,
        fontWeight: "bold"
    },
    favorite:{
        marginTop: -35,
        backgroundColor:"#fff",
        padding: 15,
        borderRadius: 100

    }
});