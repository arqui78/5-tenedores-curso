import React, {useCallback, useRef, useState} from "react"
import {View, Text, ActivityIndicator, StyleSheet} from "react-native"

import {firebaseApp} from "../utils/firebase";
import firebase from "firebase/app"
import "firebase/storage"
import Toast from "react-native-easy-toast";
import {Icon} from "react-native-elements";
import {useFocusEffect} from "@react-navigation/native";
import ListTopRestaurants from "../components/Rating/ListTopRestaurants";

const db = firebase.firestore(firebaseApp);

export default function TopRestaurants(props) {

    const {navigation} = props;

    const [ restaurants, setRestaurants ] = useState([]);
    const [ isLoading, setIsLoading ] = useState(true);

    const toastRef = useRef();

    useFocusEffect(
        useCallback(() => {
            const resultRestaurants = [];
            db.collection("restaurants")
                .orderBy("rating","desc")
                .where("rating", ">", 0)
                .limit(5)
                .get()
                .then( response => {
                    response.docs.forEach(doc => {
                        const restaurant = doc.data();
                        restaurant.id = doc.id;

                        resultRestaurants.push(restaurant)

                    })
                    setIsLoading(false)
                    setRestaurants(resultRestaurants)

                } );

        }, [])
    );

    if( !isLoading && restaurants?.length === 0) return <NotFound/>

    return (
        <View style={styles.viewBody}>
            {restaurants ? (
                <View>
                    <ListTopRestaurants
                        isLoading={isLoading}
                        restaurants={restaurants}
                        navigation={navigation}
                    />
                    <Toast ref={toastRef} position="center" opacity={0.9} />
                </View>
            ) : (
                <View style={styles.loadingRestaurants}>
                    <ActivityIndicator size="large"/>
                    <Text style={{textAlign:"center"}}>Cargando restaurantes favoritos</Text>
                </View>
            )}
        </View>
    );
  }

function NotFound(){
    return (
        <View style={styles.viewContainer}>
            <Icon type="material-community" size={50} name="alert-outline"/>
            <Text>No tiene restaurantes Favoritos</Text>
        </View>
    );
}


const styles = StyleSheet.create({
    viewContainer:{
        flex:1,
        alignItems:"center",
        justifyContent: "center"
    },
    textFound:{
        fontSize: 20,
        fontWeight: "bold"
    },
    viewBody:{
        flex:1,
        backgroundColor:"#f2f2f2f2"
    },
    loadingRestaurants:{
        marginTop: 10,
        marginBottom: 10
    },
    restaurant:{
        marginTop:10
    },
    image:{
        width: '100%',
        height: 180
    },
    info:{
        flex: 1,
        alignItems: "center",
        justifyContent: "space-between",
        flexDirection: "row",
        paddingLeft: 20,
        paddingRight: 20,
        paddingTop: 10,
        paddingBottom: 10,
        marginTop:-30,
        backgroundColor: "#fff"
    },
    name:{
        fontSize: 30,
        fontWeight: "bold"
    },
    favorite:{
        marginTop: -35,
        backgroundColor:"#fff",
        padding: 15,
        borderRadius: 100

    }
});