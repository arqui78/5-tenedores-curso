import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import {createStackNavigator, HeaderBackButton} from "@react-navigation/stack";

import BottomTabNavigator from './BottomTabNavigator';
import Restaurant from "../screens/restaurants/Restaurant";
import Login from "../screens/account/Login";
import Register from "../screens/account/Register";
const Stack = createStackNavigator();
export default function Navigation() {
  return (
    <NavigationContainer>
      <RootNavigator/>
    </NavigationContainer>
  );
}

function RootNavigator() {
  return (
      <Stack.Navigator
          screenOptions={{
              headerLeft: (props) => (
                  <HeaderBackButton
                      {...props}
                      labelVisible={false}
                  />
              )
          }}
      >
        <Stack.Screen
            name="Root"
            component={BottomTabNavigator}
            options={{headerShown: false}}
        />
        <Stack.Screen
            name="restaurant"
            initialParams={{id:null, name:"Restaurante"}}
            component={Restaurant}
        />
          <Stack.Screen
              name="login"
              component={Login}
              options={{title:"Iniciar sesión"}}
          />
          <Stack.Screen
              name="register"
              component={Register}
              options={{title:"Registro"}}
          />
      </Stack.Navigator>
  );
}

