import React from "react";
import {StyleSheet} from "react-native"

import {createStackNavigator, HeaderBackButton} from "@react-navigation/stack";
import { BlurView } from 'expo-blur';

import Restaurants from "../screens/restaurants/Restaurants";
import AddRestaurant from "../screens/restaurants/AddRestaurant";
import Restaurant from "../screens/restaurants/Restaurant";
import AddReviewRestaurant from "../screens/restaurants/AddReviewRestaurant";

const Stack = createStackNavigator();

export default function RestaurantsStack() {
  return (
      <Stack.Navigator
          screenOptions={{
              headerLeft: (props) => (
                  <HeaderBackButton
                      {...props}
                      labelVisible={false}
                  />
              )
          }}
      >
          <Stack.Screen
              name="restaurants"
              component={Restaurants}
              options={{title:"Restaurantes"}}
          />
          <Stack.Screen
              name="add-restaurant"
              component={AddRestaurant}
              options={{
                  title:"Agregar Restaurante",
              }}
          />
          {/*<Stack.Screen*/}
          {/*    name="restaurant"*/}
          {/*    component={Restaurant}*/}
          {/*    options={{*/}
          {/*        headerLeft: (props) => (*/}
          {/*            <HeaderBackButton*/}
          {/*                {...props}*/}
          {/*                // label=""*/}
          {/*                labelVisible={false}*/}
          {/*                // backImage={()=>{}}*/}
          {/*                // onPress={() => {*/}
          {/*                //     console.log(props)*/}
          {/*                //     // Do something*/}
          {/*                // }}*/}
          {/*            />*/}
          {/*        ),*/}
          {/*        // headerTransparent: true,*/}
          {/*        // headerBackground: () => (*/}
          {/*        //     <BlurView*/}
          {/*        //         tint="dark"*/}
          {/*        //         intensity={100}*/}
          {/*        //         style={StyleSheet.absoluteFill}*/}
          {/*        //     />*/}
          {/*        // ),*/}
          {/*    }}*/}
          {/*/>*/}
          <Stack.Screen
              name="add-review-restaurant"
              component={AddReviewRestaurant}
              options={{
                  title: "Nuevo comentario"
              }}
          />
      </Stack.Navigator>    
  );
}