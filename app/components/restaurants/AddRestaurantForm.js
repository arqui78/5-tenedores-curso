import React, {useState, useEffect} from "react"
import {View, StyleSheet, Text, ScrollView, Alert, Dimensions} from "react-native"
import { Input, Icon, Button, Avatar, Image } from "react-native-elements"
import * as Permissions from "expo-permissions"
import * as ImagePicker from "expo-image-picker"
import * as location from "expo-location"
import MapView from "react-native-maps"
import uuid from "random-uuid-v4"
import firebase from "firebase/app";
import "firebase/storage";
import "firebase/firestore";
import { isEmpty, size, map } from "lodash"

import {firebaseApp} from "../../utils/firebase"
import {notImage} from "../../../assets/img";
import Modal from "../commos/Modal";
const db = firebase.firestore(firebaseApp)

export default function AddRestaurantForm(props) {

    const { toastRef, setLoading, navigation } = props;
    const [ restaurantName, setRestaurantName ] = useState('');
    const [ restaurantAddress, setRestaurantAddress ] = useState('');
    const [ restaurantDescription, setRestaurantDescription ] = useState('');
    const [ restaurantLocation, setRestaurantLocation ] = useState(null);
    const [ imagesSelected, setImagesSelected ] = useState([]);
    const [ isVisibleMap, setIsVisibleMap ] = useState(false);

    const onSubmit = () =>{

        if(
            isEmpty(restaurantName) ||
            isEmpty(restaurantAddress) ||
            isEmpty(restaurantDescription)
        ){

            return toastRef.current.show('Todos los campos son requeridos.');

        }

        if(   isEmpty(restaurantLocation)  ){
            return toastRef.current.show('Tienes que localizar el restaurante en el Mapa.');
        }

        if( size(imagesSelected) === 0 ){
            return toastRef.current.show('Es requerida al menos una foto del Restaurante.');
        }

        setLoading(true);

        uploadImagesStorage().then( results => {

            db.collection("restaurants")
                .add({
                    name: restaurantName,
                    address: restaurantAddress,
                    description: restaurantDescription,
                    location: restaurantLocation,
                    images: results,
                    rating: 0,
                    ratingTotal: 0,
                    quantityVoting: 0,
                    createAt: new Date(),
                    createBy: firebase.auth().currentUser.uid
                }).then( () => {
                    setLoading(false);
                    navigation.navigate("restaurants")

                }).catch( err => {
                    setLoading(false);
                    toastRef.current.show('Error al crear el restaurante, intente mas tarde.');
                })
        })

    };

    const uploadImagesStorage = async () =>{

        const imagesBlob = [];

        await Promise.all(
            map(imagesSelected,  async image => {
                const response  = await fetch(image);
                const imageBlob = await response.blob();
                const filename =  uuid();
                const ref = firebase.storage().ref("restaurants").child(filename);
                await ref.put(imageBlob).then(async result =>{
                    // firebase.storage.ref(`restaurants/${result.metadata.name}`)
                    await firebase
                        .storage()
                        .ref(`restaurants/${filename}`)
                        .getDownloadURL()
                        .then( response => {
                            imagesBlob.push(response)
                        })

                })
            })
        );

        return imagesBlob;

    };

    return (
        <ScrollView style={styles.scrollView}>
            <ImageRestaurant
                imageRestaurant={imagesSelected[0]}
            />
            <AddForm
                setRestaurantName={setRestaurantName}
                setRestaurantAddress={setRestaurantAddress}
                setRestaurantDescription={setRestaurantDescription}
                restaurantLocation={restaurantLocation}
                setIsVisibleMap={setIsVisibleMap}
            />
            <UploadImage
                toastRef={toastRef}
                imagesSelected={imagesSelected}
                setImagesSelected={setImagesSelected}
            />
            <Button
                title="Crear Restaurante"
                buttonStyle={styles.btnOnSubmit}
                onPress={onSubmit}
            />
            <Map
                toastRef={toastRef}
                isVisibleMap={isVisibleMap}
                setIsVisibleMap={setIsVisibleMap}
                setRestaurantLocation={setRestaurantLocation}
            />
        </ScrollView>
    )
}

function AddForm(props) {

    const {
        setRestaurantName,
        setRestaurantAddress,
        setRestaurantDescription,
        restaurantLocation,
        setIsVisibleMap
    } = props;

    return(
        <View style={styles.inputsView}>
            <Input
                placeholder="Nombre del restaurante"
                containerStyle={styles.formInput}
                onChange={e => setRestaurantName(e.nativeEvent.text) }
            />
            <Input
                placeholder="Dirección"
                containerStyle={styles.formInput}
                onChange={e => setRestaurantAddress(e.nativeEvent.text) }
                rightIcon={
                    <Icon
                        type="material-community"
                        name="google-maps"
                        color={restaurantLocation ? "#00a680" : "#c2c2c2" }
                        onPress={()=> setIsVisibleMap( true )}
                    />
                }
            />
            <Input
                placeholder="Descripción del restaurante"
                containerStyle={styles.formInputTextArea}
                multiline={true}
                onChange={e => setRestaurantDescription(e.nativeEvent.text) }
            />
        </View>
    )

}

function UploadImage(props) {

    const { toastRef, imagesSelected, setImagesSelected } = props;

    const imageSelect = async () => {

        const resultPermission = await Permissions.askAsync(Permissions.CAMERA_ROLL);

        if( resultPermission.status === "denied"){
            return toastRef.current.show('Es necesario aceptar los permisos de la galeria.', 3000);
        }

        const resultImage = await ImagePicker.launchImageLibraryAsync({
            // allowsEditing:true,
            // aspect:[4,3]
        });

        if( resultImage.cancelled ){
            return toastRef.current.show('Has cerrado la selección de imagenes', 3000);
        }

        setImagesSelected([...imagesSelected, resultImage.uri])


    };

    const removeImage = image =>{

        Alert.alert(
            "Eliminar Imagen",
            "¿Estas seguro de que quieres eliminar la imagen?",
            [{
                text:"Cancelar",
                style:"cancel"
            }, {
                text:"Eliminar",
                onPress: () => setImagesSelected( imagesSelected.filter(el => el !== image) )
            }],
            {cancelable: false}
        )
    };

    return(
        <View style={styles.imagesView}>
            {
                imagesSelected.length < 4 && (
                    <Icon
                        type="material-community"
                        name="camera"
                        color="#7a7a7a"
                        containerStyle={styles.imagesViewIcon}
                        // style={styles.imagesViewIcon}
                        onPress={imageSelect}
                    />

                )
            }

            {
                imagesSelected.map((image, key)=>(
                    <Avatar
                        key={key}
                        // size="large"
                        // showEditButton
                        // onEditPress={changerAvatar}
                        // containerStyle={styles.userInfoAvatar}
                        style={styles.miniAvatar}
                        source={{ uri: image }}
                        onPress={() => removeImage(image, key)}
                    />
                ))
            }
        </View>
    )

}

function ImageRestaurant(props) {

    const { imageRestaurant } = props;

    return(
        <View style={styles.photoView}>
            <Image
                source={
                    imageRestaurant
                        ? { uri: imageRestaurant }
                        : notImage
                }
                style={{width: Dimensions.get("window").width, height: 200}}
            />
        </View>
    )

}

function Map(props) {
    const {toastRef, isVisibleMap, setIsVisibleMap, setRestaurantLocation} = props;
    const [ coords, setCoords ] = useState(null);
    const [ isChange, setIsChange ] = useState(false);

    useEffect(() =>{
        ( async ()=>{
            const resultPermissions = await Permissions.askAsync(Permissions.LOCATION)
            const statusPermissions = resultPermissions.permissions.location.status;

            if( statusPermissions !== "granted"){
                return toastRef
                    .current
                    .show('Tienes que aceptar los permisos de localización para crear un restauran.', 3000);


            }else{

                const loc = await location.getCurrentPositionAsync({});
                setCoords({
                    latitude: loc.coords.latitude,
                    longitude: loc.coords.longitude,
                    latitudeDelta: 0.001,
                    longitudeDelta: 0.001
                })
            }
        })()
    }, [setCoords]);

    const confirmLocation = () => {
        if(isChange) return;
        setRestaurantLocation(coords)
        toastRef.current.show('Localización selecciónaa correctamente.');
        setIsVisibleMap(false)
    };

    return(
        <Modal
        isVisible={isVisibleMap}
        setIsVisible={setIsVisibleMap}
        >
           <View>{
               coords ? (
                   <MapView
                       initialRegion={coords}
                       showsUserLocation
                       style={styles.mapStyle}
                       onRegionChange={ region => {
                           setCoords(region)
                           setIsChange(true)
                       } }
                       onRegionChangeComplete={() => setIsChange(false)}
                       loadingBackgroundColor="#00a680"
                       loadingEnabled
                       onPress={ e => {
                           setCoords({
                               ...coords,
                               ...e.nativeEvent.coordinate
                           })
                       }}
                   >
                       <MapView.Marker
                           coordinate={{
                               latitude: coords.latitude,
                               longitude: coords.longitude
                           }}
                           draggable
                       />
                   </MapView>
               ) : (
                   <View
                       style={styles.viewMapContainerText}
                   >
                      <Text
                          style={styles.maTextStyle}
                      >Cargando ...</Text>
                  </View>
               )}

               <View
                   style={styles.viewMapBtn}
               >

                   <Button
                       title="Cancelar"
                       containerStyle={styles.viewMapContainerBtn}
                       buttonStyle={styles.viewMapBtnCancel}
                       onPress={() => setIsVisibleMap(false)}
                   />

                   {
                       coords && (
                           <Button
                               title="Seleccionar"
                               containerStyle={styles.viewMapContainerBtn}
                               buttonStyle={styles.viewMapBtnSave}
                               onPress={() => confirmLocation()}
                               disabled={isChange}
                           />
                       )
                   }

               </View>
           </View>
        </Modal>
    )

}

const styles = StyleSheet.create({
    scrollView:{
        height:"100%"
    },
    inputsView:{
        marginLeft: 10,
        marginRight: 10
    },
    formInput:{
        marginBottom: 10
    },
    formInputTextArea:{
        // height: 10,
        width: '100%',
        padding: 0,
        margin: 0
    },
    btnOnSubmit:{
        backgroundColor:"#00a680",
        margin: 20
    },
    imagesView:{
        flexDirection:'row',
        marginTop: 30,
        marginLeft: 20,
        marginRight: 20
    },
    imagesViewIcon:{
        alignItems:"center",
        justifyContent:"center",
        marginRight: 10,
        height: 70,
        width: 70,
        backgroundColor:"#e3e3e3",
    },
    miniAvatar:{
        width: 70,
        height: 70,
        marginRight: 10,
    },
    photoView: {
        alignItems: "center",
        height: 200,
        marginBottom: 20,
    },
    mapStyle:{
        width:"100%",
        height: 450
    },
    viewMapContainerText:{
        justifyContent: "space-around",
        alignItems: "center",
        height: 400,
        color:"#00a680"
    },
    maTextStyle:{
        color:"#00a680"
    },
    viewMapBtn:{
        flexDirection: "row",
        justifyContent:"center",
        marginTop: 10
    },
    viewMapContainerBtn:{
        paddingLeft: 5,
        width: 110
    },
    viewMapBtnSave:{
        backgroundColor:"#00a680",
    },
    viewMapBtnCancel:{
        backgroundColor:"#a60d0d",
    }

});