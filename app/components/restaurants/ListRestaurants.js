import React from 'react'
import {
    StyleSheet,
    Text,
    View,
    FlatList,
    ActivityIndicator,
    TouchableOpacity
} from 'react-native'
import {Image} from "react-native-elements"
import {size, truncate} from "lodash"
import {notImage} from "../../../assets/img";
import {useNavigation, CommonActions, NavigationAction} from "@react-navigation/native";

export default function ListRestaurants(props) {
    const {isLoading, restaurants, handleLoadMore} = props;

    const navigation = useNavigation();

    return (
        <View>
            {
                size(restaurants) >  0 ? (
                    <FlatList
                        data={restaurants}
                        renderItem={ restaurant => <Restaurant restaurant={restaurant} navigation={navigation}/>}
                        keyExtractor={(item, index) => index.toString() }
                        onEndReachedThreshold={0.5}
                        onEndReached={handleLoadMore}
                        ListFooterComponent={<Loading isLoading={isLoading}/>}
                    />
                ) : (
                    <View
                        style={styles.loaderRestaurants}
                    >
                        <Loading
                            isLoading={true}
                            text="Cargando restaurantes"
                        />
                    </View>
                )
            }
        </View>
    )
}

function Restaurant(props) {

    const {restaurant, navigation, previousScreen='restaurants'} = props;
    const {
        id,
        name,
        images,
        address,
        description
    } = restaurant.item;
    const imageRestaurant = images[0];

    const goRestaurant = () =>{

        navigation.navigate('restaurant', {id, name})

    };

    return (
        <View>
            <TouchableOpacity onPress={goRestaurant} >
                <View style={styles.viewRestaurant}>
                    <View style={styles.viewRestaurantImage}>
                        <Image
                            resizeMode="cover"
                            PlaceholderContent={<ActivityIndicator color="#fff"/>}
                            style={styles.imageRestaurant}
                            source={
                                imageRestaurant
                                    ? {uri: imageRestaurant}
                                    : notImage
                            }
                        />
                    </View>
                    <View>
                        <Text style={styles.nameRestaurant}>{name}</Text>
                        <Text style={styles.addressRestaurant}>{address}</Text>
                        <Text style={styles.descriptionRestaurant}>
                            {
                                truncate(description, {
                                    length : 60,
                                    separator : ' '
                                })
                            }
                        </Text>
                    </View>
                </View>

            </TouchableOpacity>
        </View>
    )

}

function Loading(props) {
    const {isLoading, text} = props;

    return (
        <View
            style={styles.loaderRestaurants}
        >
            {
                isLoading && <ActivityIndicator size="large"/>
            }
            {
                text && (
                    <Text>{text}</Text>
                )
            }
        </View>
    )

}

const styles = StyleSheet.create({
    loaderRestaurants:{
        marginTop: 10,
        marginBottom: 10,
        alignItems:"center"

    },
    viewRestaurant:{
        flexDirection:"row",
        margin: 10,
    },
    viewRestaurantImage:{
        marginRight: 15
    },
    imageRestaurant:{
        width: 80,
        height: 80
    },
    nameRestaurant:{
        fontWeight: "bold"
    },
    addressRestaurant:{
        paddingTop: 2,
        color:"#c3c3c3"
    },
    descriptionRestaurant:{
        paddingTop: 2,
        color:"#c3c3c3",
        width: 300
    }
});

