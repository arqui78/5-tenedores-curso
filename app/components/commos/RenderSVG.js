import React, {useState, useEffect} from "react"
import {View, StyleSheet, Platform, TouchableOpacity } from "react-native"
import {SvgUri, inlineStyles, SvgFromUr, SvgWithCssUri, parse} from "react-native-svg";
import ReactDOMServer from 'react-dom/server';
import appCss from "../../../app.css";

const isWeb = Platform.OS === 'web';

const childToWeb = child => {
    const { type, props } = child;
    const name = type && type.displayName;
    const webName = name && name[0].toLowerCase() + name.slice(1);
    const Tag = webName ? webName : type;
    return <Tag {...props}>{toWeb(props.children)}</Tag>;
};

const toWeb = children => React.Children.map(children, childToWeb);


class _RenderSVG extends React.Component {
    renderSvg() {

        const {
            width='100%',
            height='100%',
            uri
        } = this.props;

        return (
            <SvgUri
                width={width}
                height={height}
                fill="green"
                fillRule="evenodd"
                uri={uri}
            />
        );
    }
    serialize = () => {
        const element = this.renderSvg();
        const webJsx = isWeb ? element : toWeb(element);
        const svgString = ReactDOMServer.renderToStaticMarkup(webJsx);
        console.log(svgString);
    };
    render() {
        return (
            <TouchableOpacity style={styles.container} onPress={this.serialize}>
                {this.renderSvg()}
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: '#ecf0f1',
        padding: 8,
    },
});

function RenderSVG(props) {

    const {
        width='100%',
        height='100%',
        uri
    } = props;

    // const svgRef = forwardRef();

    const [widthAt, setWidthAt] = useState(width);
    const [heightAt, setHeightAt] = useState(height);
    const [uriAt, setUriAt] = useState(uri);

    useEffect(()=>{
        setUriAt(uri)
    }, [uri, setUriAt]);

    return(
        <View
            className={appCss.isSelect}
            style={[
                StyleSheet.absoluteFillObject,
                {
                    alignItems: 'center',
                    justifyContent: 'center',
                    fill:"green"
                },
            ]}>

            <SvgUri
                width={widthAt}
                height={heightAt}
                // fill="green"
                fillRule="evenodd"
                uri={uriAt}
                // style={appCss.isSelect}
            />

        </View>
    )
}

export default RenderSVG
