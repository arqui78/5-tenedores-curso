import React from 'react'
import {
    StyleSheet,
    Text,
    View,
    FlatList,
    ActivityIndicator,
    TouchableOpacity
} from 'react-native'
import {Card, Icon, Image, Rating} from "react-native-elements"
import {size, truncate} from "lodash"
import {notImage} from "../../../assets/img";
import {useNavigation, CommonActions, NavigationAction} from "@react-navigation/native";

export default function ListRestaurants(props) {
    const {isLoading, restaurants} = props;

    const navigation = useNavigation();

    return (
        <View>
            {
                size(restaurants) >  0 ? (
                    <FlatList
                        data={restaurants}
                        renderItem={ restaurant => <Restaurant restaurant={restaurant} navigation={navigation}/>}
                        keyExtractor={(item, index) => index.toString() }
                        onEndReachedThreshold={0.5}
                        // onEndReached={handleLoadMore}
                        ListFooterComponent={<Loading isLoading={isLoading}/>}
                    />
                ) : (
                    <View
                        style={styles.loaderRestaurants}
                    >
                        <Loading
                            isLoading={true}
                            text="Cargando restaurantes"
                        />
                    </View>
                )
            }
        </View>
    )
}

function Restaurant(props) {

    const {restaurant, navigation, index} = props;
    const {
        id,
        name,
        images,
        rating,
        description
    } = restaurant.item;
    const imageRestaurant = images[0];
    const arrayColors = ['#efb819', '#e3e4e5', '#cd7f32', '#000', '#000']

    const goRestaurant = () => navigation.navigate('restaurant', {id, name});

    return (
        <View>
            <TouchableOpacity onPress={goRestaurant} >
                <Card
                    containerStyle={styles.containerCard}
                >
                    <Icon
                        type="material-community"
                        name="chess-queen"
                        color={arrayColors[restaurant.index] || "#000"}
                        containerStyle={styles.containerIcon}
                    />
                    <Image
                        resizeMode="cover"
                        PlaceholderContent={<ActivityIndicator color="#fff"/>}
                        style={styles.imageRestaurant}
                        source={
                            imageRestaurant
                                ? {uri: imageRestaurant}
                                : notImage
                        }
                    />
                    <View style={styles.titleRating}>
                        <Text style={styles.titleText}>{name}</Text>
                        <Rating
                            imageSize={20}
                            startingValue={rating}
                            readonly
                        />
                    </View>
                    <Text style={styles.description}>{description}</Text>
                </Card>
            </TouchableOpacity>
        </View>
    )

}

function Loading(props) {
    const {isLoading, text} = props;

    return (
        <View
            style={styles.loaderRestaurants}
        >
            {
                isLoading && <ActivityIndicator size="large"/>
            }
            {
                text && (
                    <Text>{text}</Text>
                )
            }
        </View>
    )

}

const styles = StyleSheet.create({
    loaderRestaurants:{
        marginTop: 10,
        marginBottom: 10,
        alignItems:"center"
    },
    containerCard:{
        marginBottom: 30,
        borderWidth: 0
    },
    containerIcon:{
        position:"absolute",
        top: -30,
        left:-30,
        zIndex:1
    },
    imageRestaurant:{
        width: "100%",
        height: 200
    },
    titleRating: {
        flexDirection:"row",
        marginTop: 10,
        justifyContent:"space-between"
    },
    titleText:{
        fontSize:20,
        fontWeight:"bold"
    },
    description:{
        color:"grey",
        marginTop:0,
        textAlign:"justify"
    }
});

