import React, {useState} from "react"
import {View, StyleSheet, Text} from "react-native"
import { Input, Icon, Button } from "react-native-elements"
import { size, isEmpty } from "lodash"
import * as firebase from "firebase"
import {useNavigation} from "@react-navigation/native";

import { validateEmail } from "../../utils/validations"
import Loading from "../commos/Loading";
import styles_css from "../../../app.css";



export default function RegisterForm(props) {

    const { toastRef } = props;

    const [ showPassword, setShowPassword ] = useState(false)
    const [ showRepeatPassword, setShowRepeatPassword ] = useState(false)
    const [ formData, setFormData ] = useState(defaultFormData())
    const [ loading, setLoading ] = useState(false)

    const navigation = useNavigation()

    const onSubmit = () =>{

        if(
            isEmpty( formData.email ) ||
            isEmpty( formData.password ) ||
            isEmpty( formData.repeatPassword )
        ){

            toastRef.current.show('Todos los campos son obligatorios')

            return;

        }

        if( !validateEmail(formData.email) ) {

            toastRef.current.show('Email invalido')
            return;
        }

        if( formData.password !== formData.repeatPassword ){

            toastRef.current.show('Las contraseña deben ser iguales')
            return;
        }

        if( size(formData.password) < 6 ){

            toastRef.current.show('Las contraseña tiene que tener al menos 6 caracteres')
            return;

        }

        setLoading(true)

        firebase
            .auth()
            .createUserWithEmailAndPassword(formData.email, formData.password)
            .then( response => {
                setLoading(false)
                navigation.navigate('account')
            } )
            .catch( err => {
                setLoading(false)
                toastRef.current.show('El email, ya esta en uso, prueba con otro')
            })

    };

    const onChange = (e, type) =>{

        setFormData({ ...formData, [type]: e.nativeEvent.text })

    };

    return (
        <View style={styles.formContainer}>
            <Loading isVisible={loading} text="Creando cuenta"/>
            <Input
                placeholder="Correo electrónico"
                containerStyle={styles.inputForm}
                onChange={e => onChange(e, 'email')}
                rightIcon={
                    <Icon
                        type="material-community"
                        name="at"
                        iconStyle={styles.iconRight}
                    />
                }
            />
            <Input
                placeholder="Contraseña"
                containerStyle={styles.inputForm}
                password={true}
                secureTextEntry={showPassword ? false : true}
                onChange={e => onChange(e, 'password')}
                rightIcon={
                    <Icon
                        type="material-community"
                        name={showPassword ? "eye-off-outline" : "eye-outline" }
                        iconStyle={styles.iconRight}
                        onPress={()=> setShowPassword( !showPassword)}
                    />
                }
            />
            <Input
                placeholder="Repetir Contraseña"
                containerStyle={styles.inputForm}
                password={true}
                secureTextEntry={showRepeatPassword ? false : true}
                onChange={e => onChange(e, 'repeatPassword')}
                passwordRules={{minlength: 8}}
                rightIcon={
                    <Icon
                        type="material-community"
                        name={showRepeatPassword ? "eye-off-outline" : "eye-outline" }
                        iconStyle={styles.iconRight}
                        onPress={()=> setShowRepeatPassword( prevState => !prevState)}
                    />
                }
            />
            <Button
                title="Ubirse"
                className={styles_css.btnPrimary}
                containerStyle={styles.btnContainerRegister}
                // buttonStyle={styles.btnRegister}
                buttonStyle={styles_css.buttonStylePrimary}
                onPress={onSubmit}
            />
        </View>
    )
}

function defaultFormData() {
    return{
        email:"",
        password:"",
        repeatPassword:""
    }
}

const styles = StyleSheet.create({
    formContainer:{
        flex:1,
        alignItems:"center",
        justifyContent:"center",
        marginTop: 30
    },
    inputForm:{
        width:"100%",
        marginTop: 20
    },
    btnContainerRegister:{
        marginTop: 20,
        width:"95%"
    },
    btnRegister:{
        backgroundColor:"#00a680"
    },
    iconRight:{
       color:"#c1c1c1"
    }
})