import React, {useState} from "react"
import {View, StyleSheet } from "react-native"
import {Button, Icon, Input} from "react-native-elements";
import {isEmpty} from "lodash";
import * as firebase from "firebase";
import { validateEmail } from "../../utils/validations";
import { reauthenticate } from "../../utils/api";

export default function ChangeEmailForm(props) {

    const {
        toastRef,
        email,
        setShowModal,
        setReloadUserInfo
    } = props;

    const [ isLoading, setIsLoading ] = useState(false);
    const [ showPassword, setShowPassword ] = useState(false)
    const [ formData, setFormData ] = useState(defaultFormData(email))
    const [ errors, setErrors ] = useState({});

    const onChange = (e, type) =>{
        setErrors({})
        setFormData({ ...formData, [type]: e.nativeEvent.text })

    };

    const onSubmit = () =>{

        if( isEmpty( formData.email ) ){

            setErrors({
                email: 'El email es requerido'
            });

            return;

        }

        if( formData.email === email ){

            setErrors({
                email: 'El email no ha cambiado'
            });
            return;
        }



        if( isEmpty( formData.password ) ){

            setErrors({
                password: 'La contraseña es requerida'
            });

            return;

        }



        if( !validateEmail(formData.email) ) {

            setErrors({
                email: 'Email invalido'
            });
            return;
        }

        setIsLoading(true);

        reauthenticate( formData.password )
            .then( () =>{

            firebase
                .auth()
                .currentUser
                .updateEmail(formData.email)
                .then( () => {
                    setIsLoading(false);
                    setReloadUserInfo(true);
                    toastRef.current.show('Email actualizado correctamente')
                    setShowModal(false);
                } )
                .catch( err => {
                    setIsLoading(false)
                    setErrors({email: 'Error al actualizar el correo electrónico'})
                })

        }).catch(err =>{
            setIsLoading(false);
            setErrors({
                password: 'Contraseña no es correcta.'
            });
        });

    };

    return (
        <View style={styles.view}>
            <Input
                placeholder="Correo electrónico"
                containerStyle={styles.input}
                rightIcon={
                    <Icon
                        type="material-community"
                        name="at"
                        color="#c2c2c2"
                    />
                }
                defaultValue={email || ''}
                onChange={ e => onChange(e, 'email')}
                errorMessage={errors.email}
            />
            <Input
                placeholder="Contraseña"
                containerStyle={styles.input}
                password={true}
                secureTextEntry={showPassword ? false : true}
                rightIcon={
                    <Icon
                        type="material-community"
                        name={showPassword ? "eye-off-outline" : "eye-outline" }
                        onPress={()=> setShowPassword( !showPassword)}
                        color="#c2c2c2"
                    />
                }
                onChange={ e => onChange(e, 'password')}
                errorMessage={errors.password}
            />
            <Button
                title="Cambiar email"
                containerStyle={styles.btnContainer}
                buttonStyle={styles.btn}
                onPress={onSubmit}
                loading={isLoading}
            />
        </View>
    )

}

function defaultFormData(email) {
    return{
        email    : email,
        password : "",

    }
}

const styles = StyleSheet.create({
    view:{
        alignItems:"center",
        paddingTop: 10,
        paddingBottom: 10
    },
    input:{
        marginBottom: 10
    },
    btnContainer:{
        marginTop:20,
        width:"95%"
    },
    btn:{
        backgroundColor:"#00a680"
    }
});