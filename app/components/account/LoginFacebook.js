import React, {useState} from "react"
import {StyleSheet, View} from "react-native"
import { SocialIcon } from "react-native-elements"
import * as firebase from "firebase"
import * as Facebook from "expo-facebook"
import {FacebookApi} from "../../utils/social"
import {useNavigation} from "@react-navigation/native";
import Loading from "../commos/Loading";


export default function LoginFacebook(props) {

    const { toastRef } = props;
    const navigation = useNavigation();
    const [ loading, setLoading ] = useState(false)

    const login = async () =>{
        try {

            await Facebook.initializeAsync(FacebookApi.application_id);
            const result = await Facebook.logInWithReadPermissionsAsync({
                permissions: FacebookApi.permissions,
            });

            const {type, token } = result;


            switch (type) {

                case "success":
                    setLoading(true)
                    const credentials = firebase.auth.FacebookAuthProvider.credential(token)
                    firebase
                        .auth()
                        .signInWithCredential(credentials)
                        .then( () => {
                            setLoading(false)
                            navigation.navigate('account')
                        } )
                        .catch( err => {
                            setLoading(false)
                            toastRef.current.show('Credenciales incorrectas')
                        });
                    break
                case "cancel":
                    toastRef.current.show('Inicio de sesión cancelado')
                    break
                default:
                    toastRef.current.show('Error desconocido, intentelo mas tarde')
            }

        } catch ({ message }) {
            alert(`Facebook Login Error: ${message}`);
        }
    }
    return (
        <>
            <SocialIcon
                title="Iniciar sesión con Facebook"
                button
                type="facebook"
                onPress={login}
            />
            <Loading isVisible={loading} text="Iniciando sesión"/>
        </>

    )
}

const styles = StyleSheet.create({
    formContainer:{
        flex:1,
        alignItems:"center",
        justifyContent:"center",
        marginTop: 30
    },
    inputForm:{
        width:"100%",
        marginTop: 20
    },
    btnContainerRegister:{
        marginTop: 20,
        width:"95%"
    },
    btnRegister:{
        backgroundColor:"#00a680"
    },
    iconRight:{
        color:"#c1c1c1"
    }
});
