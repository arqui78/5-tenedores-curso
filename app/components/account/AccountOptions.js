import React, {useState} from "react"
import {View, StyleSheet} from "react-native"
import { ListItem } from "react-native-elements"

import Modal from "../commos/Modal";
import ChangeDisplayNameForm from "./ChangeDisplayNameForm";
import ChangeEmailForm from "./ChangeEmailForm";
import ChangePasswordForm from "./ChangePasswordForm";



export default function AccountOptions(props) {

    const {
        userInfo,
        toastRef,
        setReloadUserInfo
    } = props;

    const [showModal, setShowModal] = useState(false);
    const [renderComponent, setRenderComponent] = useState(null);


    const selectComponent = key =>{

        try{

            switch (key) {
                case "displayName":
                    setRenderComponent(
                        <ChangeDisplayNameForm
                            displayName={userInfo.displayName}
                            toastRef={toastRef}
                            setShowModal={setShowModal}
                            setReloadUserInfo={setReloadUserInfo}
                        />
                    );
                    setShowModal(true);
                    break;
                case "displayEmail":
                    setRenderComponent(
                        <ChangeEmailForm
                            email={userInfo.email}
                            toastRef={toastRef}
                            setShowModal={setShowModal}
                            setReloadUserInfo={setReloadUserInfo}
                        />
                    );
                    setShowModal(true)
                    break;
                case "displayPassword":
                    setRenderComponent(
                        <ChangePasswordForm
                            toastRef={toastRef}
                            setShowModal={setShowModal}
                        />
                    );
                    setShowModal(true)
                    break;
                default:
                    setRenderComponent(null);
                    setShowModal(false);

            }

        }catch (e) {
            console.log(e)
        }

    };

    const menuOptions = generateOptions(selectComponent);

    return (
        <View>
            {
                renderComponent && (
                    <Modal
                        isVisible={showModal}
                        setIsVisible={setShowModal}
                    >
                        {renderComponent}
                    </Modal>
                )

            }

            {
                menuOptions.map((menu, key)=>(
                    <ListItem
                        key={key}
                        title={menu.title}
                        leftIcon = {{
                            type: menu.iconType,
                            name: menu.iconNameLeft,
                            color: menu.iconColorLeft
                        }}
                        rightIcon = {{
                            type: menu.iconType,
                            name: menu.iconNameRight,
                            color: menu.iconColorRight
                        }}
                        containerStyle={styles.menuItem}
                        disabled={userInfo ? false : true}
                        onPress={menu.onPress}
                    />
                ))
            }
        </View>
    )
}

function generateOptions(onSelectComponent) {
    return[{
        title:" Cambiar Nombre y Apellido",
        iconType:"material-community",
        iconNameLeft: "account-circle",
        iconColorLeft:"#ccc",
        iconNameRight: "chevron-right",
        iconColorRight:"#ccc",
        onPress: () => onSelectComponent('displayName')

    },{
        title:" Cambiar Email",
        iconType:"material-community",
        iconNameLeft: "at",
        iconColorLeft:"#ccc",
        iconNameRight: "chevron-right",
        iconColorRight:"#ccc",
        onPress: () => onSelectComponent('displayEmail')
    },{
        title:" Cambiar contraseña",
        iconType:"material-community",
        iconNameLeft: "lock-reset",
        iconColorLeft:"#ccc",
        iconNameRight: "chevron-right",
        iconColorRight:"#ccc",
        onPress: () => onSelectComponent('displayPassword')
    }];
}

const styles = StyleSheet.create({
    menuItem:{
        borderBottomWidth: 1,
        borderBottomColor: "#e3e3e3"
    }
})