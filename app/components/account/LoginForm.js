import React, {useState} from "react"
import {View, StyleSheet} from "react-native"
import { Input, Icon, Button } from "react-native-elements"
import { isEmpty } from "lodash"
import * as firebase from "firebase"
import {useNavigation} from "@react-navigation/native";


import { validateEmail } from "../../utils/validations"
import Loading from "../commos/Loading";


export default function LoginForm(props) {

    const { toastRef } = props;

    const [ showPassword, setShowPassword ] = useState(false)
    const [ formData, setFormData ] = useState(defaultFormData())
    const [ loading, setLoading ] = useState(false)

    const navigation = useNavigation()

    // firebase.auth().onAuthStateChanged(user =>{
    //     if( user ) navigation.navigate('account')
    // })

    const onSubmit = () =>{

        if(
            isEmpty( formData.email ) ||
            isEmpty( formData.password )
        ){

            toastRef.current.show('Todos los campos son obligatorios')

            return;

        }

        if( !validateEmail(formData.email) ) {

            toastRef.current.show('Email invalido')
            return;
        }

        setLoading(true)

        firebase
            .auth()
            .signInWithEmailAndPassword(formData.email, formData.password)
            .then( response => {
                setLoading(false)
                // navigation.navigate('account')
                navigation.goBack()
            } )
            .catch( err => {
                setLoading(false)
                toastRef.current.show('Email y/o contraseña son invalidas')
            })

    };

    const onChange = (e, type) =>{
        setFormData({ ...formData, [type]: e.nativeEvent.text.trim() })
    };

    return (
        <View style={styles.formContainer}>
            <Input
                placeholder="Correo electrónico"
                containerStyle={styles.inputForm}
                onChange={e => onChange(e, 'email')}
                autoCapitalize = 'none'
                textContentType='emailAddress'
                keyboardType='email-address'
                autoCorrect={false}
                autoCompleteType='email'
                rightIcon={
                    <Icon
                        type="material-community"
                        name="at"
                        iconStyle={styles.iconRight}
                    />
                }
            />
            <Input
                placeholder="Contraseña"
                containerStyle={styles.inputForm}
                password={true}
                secureTextEntry={showPassword ? false : true}
                onChange={e => onChange(e, 'password')}
                rightIcon={
                    <Icon
                        type="material-community"
                        name={showPassword ? "eye-off-outline" : "eye-outline" }
                        iconStyle={styles.iconRight}
                        onPress={()=> setShowPassword( !showPassword)}
                    />
                }
            />
            <Button
                title="Enviar"
                containerStyle={styles.btnContainerRegister}
                buttonStyle={styles.btnRegister}
                onPress={onSubmit}
            />
            <Loading isVisible={loading} text="Iniciando sesión"/>
        </View>
    )
}

function defaultFormData() {
    return{
        email:"",
        password:""
    }
}

const styles = StyleSheet.create({
    formContainer:{
        flex:1,
        alignItems:"center",
        justifyContent:"center",
        marginTop: 20
    },
    inputForm:{
        width:"100%",
        marginTop: 15
    },
    btnContainerRegister:{
        marginTop: 15,
        width:"95%"
    },
    btnRegister:{
        backgroundColor:"#00a680"
    },
    iconRight:{
       color:"#c1c1c1"
    }
});