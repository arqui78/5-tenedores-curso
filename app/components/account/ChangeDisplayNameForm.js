import React, {useState} from "react"
import {View, StyleSheet } from "react-native"
import {Button, Icon, Input} from "react-native-elements";
import {isEmpty} from "lodash";
import * as firebase from "firebase";

export default function ChangeDisplayNameForm(props) {

    const {
        toastRef,
        displayName,
        setShowModal,
        setReloadUserInfo
    } = props;

    const [ isLoading, setIsLoading ] = useState(false)
    const [ newDisplayName, setNewDisplayName ] = useState(displayName)
    const [ error, setError ] = useState(null)

    const onChange = (e, type) =>{
        setError(null)
        setNewDisplayName(e.nativeEvent.text)

    };

    const onSubmit = () =>{

        if( isEmpty( newDisplayName ) ){

            setError('El nombre y apellido es requerido')

            return;

        }

        if( displayName === newDisplayName){
            setError('El nombre y apellido no ha cambiado');
            return;
        }

        setIsLoading(true);

        const update = {
            displayName: newDisplayName
        };


        firebase
            .auth()
            .currentUser
            .updateProfile(update)
            .then( response => {
                setIsLoading(false);
                setShowModal(false);
                setReloadUserInfo(true)
            } )
            .catch( err => {
                setIsLoading(false)
                setError('Error al actualizar el nombre')

            })

    };

    return (
        <View style={styles.view}>
            <Input
                placeholder="Nombre y Apellido"
                containerStyle={styles.input}
                rightIcon={
                    <Icon
                        type="material-community"
                        name="account-circle-outline"
                        color="#c2c2c2"
                    />
                }
                defaultValue={displayName || ''}
                onChange={onChange}
                errorMessage={error}
            />
            <Button
                title="Cambiar nombre"
                containerStyle={styles.btnContainer}
                buttonStyle={styles.btn}
                onPress={onSubmit}
                loading={isLoading}
            />
        </View>
    )

}

const styles = StyleSheet.create({
    view:{
        alignItems:"center",
        paddingTop: 10,
        paddingBottom: 10
    },
    input:{
        marginBottom: 10
    },
    btnContainer:{
        marginTop:20,
        width:"95%"
    },
    btn:{
        backgroundColor:"#00a680"
    }
});