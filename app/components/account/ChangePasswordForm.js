import React, {useState} from "react"
import {View, StyleSheet } from "react-native"
import {Button, Icon, Input} from "react-native-elements";
import {isEmpty, size} from "lodash";
import * as firebase from "firebase";
import {reauthenticate} from "../../utils/api";

export default function ChangePasswordForm(props) {

    const {
        toastRef,
        setShowModal
    } = props;

    const [ isLoading, setIsLoading ] = useState(false)
    const [ showCurrentPassword, setShowCurrentPassword ] = useState(false)
    const [ showNewPassword, setNewShowPassword ] = useState(false)
    const [ showRepeatPassword, setShowRepeatPassword ] = useState(false)
    const [ formData, setFormData ] = useState(defaultFormData())
    const [ errors, setErrors ] = useState({});

    const onChange = (e, type) =>{
        setErrors({})
        setFormData({ ...formData, [type]: e.nativeEvent.text })

    };

    const onSubmit = () =>{

        let isSetErrors = true;

        if(
            isEmpty( formData.currentPassword ) ||
            isEmpty( formData.newPassword ) ||
            isEmpty( formData.repeatPassword )
        ){

            setErrors({
                currentPassword : !formData.currentPassword ? 'El campo es requerido' : '',
                newPassword     : !formData.newPassword ? 'El campo es requerido' : '',
                repeatPassword  : !formData.repeatPassword ? 'El campo es requerido' : ''
            });

            return;

        }



        if( formData.newPassword !== formData.repeatPassword ){

            setErrors({
                newPassword: 'Las contraseña deben ser iguales',
                repeatPassword: 'Las contraseña deben ser iguales'
            });

            return;
        }

        if( size(formData.newPassword) < 6 ){

            setErrors({
                newPassword: 'Las contraseña tiene que tener al menos 6 caracteres',
            });

            return;

        }

        if( size(formData.repeatPassword) < 6 ){

            setErrors({
                newPassword: 'Las contraseña tiene que tener al menos 6 caracteres',
            });

            return;

        }

        setIsLoading(true);

        reauthenticate( formData.currentPassword )
            .then( response =>{

                // console.log(response)
                firebase
                    .auth()
                    .currentUser
                    .updatePassword(formData.newPassword)
                    .then( () => {
                        setIsLoading(false);
                        setShowModal(false);
                        toastRef.current.show('Contraseña actualizada correctamente')
                        firebase.auth().signOut();
                    } )
                    .catch( err => {
                        setIsLoading(false)
                        setErrors({email: 'Error al actualizar la contraseña'})
                    })

            }).catch(err =>{
                console.log(err)
                setIsLoading(false);
                setErrors({
                    currentPassword: 'Contraseña no es correcta.'
                });
        });



    };

    return (
        <View style={styles.view}>
            <Input
                placeholder="Contraseña actual"
                containerStyle={styles.input}
                password={true}
                secureTextEntry={showCurrentPassword ? false : true}
                rightIcon={
                    <Icon
                        type="material-community"
                        name={showCurrentPassword ? "eye-off-outline" : "eye-outline" }
                        onPress={()=> setShowCurrentPassword( !showCurrentPassword)}
                        color="#c2c2c2"
                    />
                }
                onChange={ e => onChange(e, 'currentPassword')}
                errorMessage={errors.currentPassword}
            />
            <Input
                placeholder="Nueva Contraseña"
                containerStyle={styles.input}
                password={true}
                secureTextEntry={showNewPassword ? false : true}
                rightIcon={
                    <Icon
                        type="material-community"
                        name={showNewPassword ? "eye-off-outline" : "eye-outline" }
                        onPress={()=> setNewShowPassword( prevState => !prevState)}
                        color="#c2c2c2"
                    />
                }
                onChange={e => onChange(e, 'newPassword')}
                errorMessage={errors.newPassword}
            />
            <Input
                placeholder="Repetir Contraseña"
                containerStyle={styles.input}
                password={true}
                secureTextEntry={showRepeatPassword ? false : true}
                rightIcon={
                    <Icon
                        type="material-community"
                        name={showRepeatPassword ? "eye-off-outline" : "eye-outline" }
                        onPress={()=> setShowRepeatPassword( prevState => !prevState)}
                        color="#c2c2c2"
                    />
                }
                onChange={e => onChange(e, 'repeatPassword')}
                errorMessage={errors.repeatPassword}
            />
            <Button
                title="Cambiar contraseña"
                containerStyle={styles.btnContainer}
                buttonStyle={styles.btn}
                onPress={onSubmit}
                loading={isLoading}
            />
        </View>
    )

}

function defaultFormData() {
    return{
        currentPassword:"",
        newPassword:"",
        repeatPassword:""
    }
}

const styles = StyleSheet.create({
    view:{
        alignItems:"center",
        paddingTop: 10,
        paddingBottom: 10
    },
    input:{
        marginBottom: 10
    },
    btnContainer:{
        marginTop:20,
        width:"95%"
    },
    btn:{
        backgroundColor:"#00a680"
    }
});