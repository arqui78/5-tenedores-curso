import React from 'react';
import { StatusBar } from 'expo-status-bar';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { YellowBox } from 'react-native'
import base64 from 'base-64';


import Navigation from './app/navigations/Navigation';

if(!global.btoa) global.btoa = base64.encode;
if(!global.atob) global.atob = base64.decode;

export default function App() {

  YellowBox.ignoreWarnings([
    'Animated: `useNativeDriver` was not specified. This is a required option and must be explicitly set to `true` or `false`',
    'Setting a timer'
  ])

  return (
      <>
        <Navigation/>
        <StatusBar/>
      </>
  );
}
